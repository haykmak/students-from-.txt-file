﻿using Student_list_from_file;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_list_from_file
{
    public static class FileInfo_Extensions
    {
        public static void Append(this FileInfo file, List<Student> students)
        {
            StreamWriter writer = file.AppendText();

            foreach (var student in students)
            {
                var arr = student.GetType().GetProperties();
                foreach (var item in arr)
                {
                    writer.WriteLine($"{item.Name}: {item.GetValue(student)}");
                }
                writer.WriteLine();
            }
            writer.Close();

        }

        public static void Append(this FileInfo file, Student student)
        {
            StreamWriter writer = file.AppendText();

            var arr = student.GetType().GetProperties();
            foreach (var item in arr)
            {
                writer.WriteLine($"{item.Name}: {item.GetValue(student)}");
            }
            writer.WriteLine();

            writer.Close();
        }

        


    }
}
