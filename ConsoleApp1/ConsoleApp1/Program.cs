﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Student_list_from_file
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = new FileInfo(@"D:\Students.txt");

            List<Student> students = CreateStudents(3).ToList();
            file.Append(students);


            Student[] studentsArr = FileContentToCollection(file).ToArray();
            Console.ReadKey();

        }




        static IEnumerable<Student> FileContentToCollection(FileInfo file)
        {
            string[] fileLines = File.ReadAllLines(file.FullName);
            
            List<string[]> groupedInfos = GroupStudentsInfo(fileLines);

            foreach (var arr in groupedInfos)
            {
                Student st = new Student();
                var properties = st.GetType().GetProperties();

                for (int i = 0; i < arr.Length; i++)
                {
                    string[] array = arr[i].Split(':',' ');
                    switch (array[0].ToUpper())
                    {
                        case "NAME":
                            st.Name = array.Last();
                            break;
                        case "SURNAME":
                            st.Surname = array.Last();
                            break;
                        case "AGE":
                            st.Age = Convert.ToByte(array.Last());
                            break;
                        case "UNIVERSITY":
                            st.University = array.Last();
                            break;
                        default:
                            Console.WriteLine($"Students don't have {array[0]} property");
                            break;

                    }

                }
                yield return st;
               
            }

            

        }

        private static List<string[]> GroupStudentsInfo(string[] studentsInfo)
        {
            List<string[]> groupedInfos = new List<string[]>();
            while (studentsInfo.Length != 0)
            {
                string[] temp = studentsInfo.TakeWhile(p => p != "").ToArray();
                studentsInfo = studentsInfo.Skip(temp.Length).ToArray();
                while (studentsInfo.Length>0 && studentsInfo[0] == "")
                {
                    studentsInfo = studentsInfo.Skip(1).ToArray();
                }
                groupedInfos.Add(temp);
            }

            return groupedInfos;

        }

        static IEnumerable<Student> CreateStudents(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new Student
                {
                    Name = $"A{(i + 1)}",
                    Surname = $"A{(i + 1)}yan",
                    Age = (byte)rnd.Next(20, 35),
                    University = Convert.ToString((University)rnd.Next(0, 3))
                };
            }
        }

    }
}
