﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_list_from_file
{
    public class Student
    {
        
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte? Age { get; set; }
        public String University { get ; set; }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }
    }

    public enum University
    {
        Politexnik,
        PetHamalsaran,
        Mankavarjakan
    }

}
